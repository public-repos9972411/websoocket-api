from typing import List
from pydantic import BaseModel
from datetime import datetime as dt


class Transactions(BaseModel):
    order_id: int
    items: List[str]
    amount: float
    serial_id: str
    type: str
    terminal_id: int
    timestamp: dt | None
    processed: bool = False
    Retrieved: bool = False

    class Config:
        from_attributes = True
        populate_by_name = True
        arbitrary_types_allowed = True
        json_schema_extra = {
                            "example": 
                                {
                                "order_id": "890905567",
                                "items": ["item1", "item2"],
                                "amount": 100.00,
                                "serial_id": "8899",
                                "type": "transaction",
                                "terminal_id": 8010,
                                "timestamp" : "2023-11-02T11:36:03.307Z",
                                "processed": False,
                                "Retrieved": False
                                }
                            }    
        
