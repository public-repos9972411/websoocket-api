
from pydantic import BaseModel
from datetime import datetime as dt
from enum import Enum


class TerminalCurrency(str, Enum):
    GBP = "GBP"
    EUR = "EUR"

    @classmethod
    def get_symbol(cls, currency: str) -> str:
        try:
            return cls[currency.upper()].value
        except KeyError as e:
            raise Exception(f"Unable to fetch status {e}")
        
        
class Terminals(BaseModel):
    mid: int | None
    tid: int 
    currency: TerminalCurrency | None
    timestamp: dt | None
    vhost_name:  str 
    cid: str | None
    registered: bool | False

    class Config:
        from_attributes = True
        populate_by_name = True
        arbitrary_types_allowed = True
        json_schema_extra = {
                            "example": 
                                    {
                                    "mid": 1234556,
                                    "tid": 8010,
                                    "currency": "GBP",
                                    "timestamp" : "2023-11-02T11:36:03.307Z",
                                    "vhost_name": "vhost_8010",
                                    "cid": "3155",
                                    "registered": "False"
                                    }
                            }    
