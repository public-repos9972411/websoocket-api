from typing import List
from pydantic import BaseModel
from datetime import datetime as dt


class TerminalsData(BaseModel):
    tid: int
    mid: int | None


class EposData(BaseModel):
    serialid: str
    terminals: List[TerminalsData]
    timestamp: dt | None 
    cid: str | None
    vhost_name: str | None

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        allow_population_by_field_name = True
        json_encoders = {
            dt: lambda v: v.isoformat(),
        }
        json_decoders = {
            dt: lambda v: dt.fromisoformat(v),
        }
        json_schema_extra = {
                            "example": 
                                [
                                {
                                "serialid": "8899",
                                "terminals": [{"tid": 8010, "mid": 1234556},
                                            {"tid": 8011, "mid": 1234556},
                                            {"tid": 8012, "mid": 1234556}
                                         ],
                                "timestamp" : "2023-11-12T15:36:03.307Z",
                                "cid": "3155",
                                "vhost_name": "vhost_3155"
                                },
                                {
                                "serialid": "8900",
                                "terminals": [{"tid": 8010, "mid": 1234556},
                                            {"tid": 8011, "mid": 1234556},
                                            {"tid": 8012, "mid": 1234556}
                                            ],
                                "timestamp" : "2023-11-12T15:36:03.307Z",
                                "cid": "3155",
                                "vhost_name": "vhost_3155"
                                },
                                {
                                "serialid": "4090",
                                "terminals": [{"tid": 4010, "mid": 4234556},
                                            {"tid": 4011, "mid": 4234556},
                                            {"tid": 4012, "mid": 4234556}
                                            ],
                                "timestamp" : "2023-11-12T15:36:03.307Z",
                                "cid": "4155",
                                "vhost_name": ""
                                },
                                {
                                 "serialid": "4091",
                                "terminals": [{"tid": 4013, "mid": 4234556},
                                            {"tid": 4014, "mid": 4234556},
                                            {"tid": 4015, "mid": 4234556}
                                            ],
                                "timestamp" : "2023-11-12T15:36:03.307Z",
                                "cid": "4155",
                                "vhost_name": ""
                                }                           
                            ]
                            }