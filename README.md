# pylon

## Getting started

This is an API that faciliates the communication between a Card Machine referred to as a Terminal, a gateway and an EPOS system.

***

## Name
EPOS, Terminal API using websockets and REST API.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
More details will be provided soon.

## Contributing
Feel free to fork the repo and contact me.

## Authors and acknowledgment
Lwanga Nyambare

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
