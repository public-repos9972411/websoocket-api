# Description: This is the main file for the terminal. 
import asyncio
import logging
import sys
sys.path.append('/Users/lwanga/Documents/HARK/pylon')
from utils import TerminalSystem



# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
async def main(terminal_id):
    url = f"ws://localhost:8000/ws/terminal/{terminal_id}"
    terminal_system = TerminalSystem(terminal_id, url)
    monitor_task = None
    periodic_task = None

    try:
        print("Starting Terminal application...")
        await terminal_system.websocket_init()
        await terminal_system.redis_init()
        monitor_task = asyncio.create_task(terminal_system.ws_reconnect())

        if not await terminal_system.check_terminal_registration():
            return
        periodic_task = asyncio.create_task(terminal_system.send_periodic_ping())
        await terminal_system.terminal_endpoint()
    
    except KeyboardInterrupt:
        print("Keyboard Interrupt. Exiting...")
        await terminal_system.close_connections()
    except asyncio.exceptions.CancelledError:
        print("Terminal application interrupted. Cleaning up...")
        await terminal_system.close_connections()
    except Exception as e:
        print(f"Unexpected error: {e}")
        await terminal_system.close_connections()
    finally:
        if monitor_task:
            monitor_task.cancel()
            periodic_task.cancel()
        # await terminal_system.close_connections()
        print("Terminal application shutdown.")
        

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python terminal.py <TERMINAL_ID>")
        sys.exit(1)
    try:
        terminal_id = int(sys.argv[1])
    except ValueError:
        print("Invalid TERMINAL_ID. Please enter a valid integer.")
        sys.exit(1)

    asyncio.run(main(terminal_id))

