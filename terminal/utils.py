
import asyncio
import json
import logging
import os
import random
import sys
import websockets
from datetime import datetime as dt
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
from db.rabbitmq_c import RabbitMQConnectionManager
from db.redis import redis_connection
from common.utils_client import WebSocketClient
from dotenv import load_dotenv
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

# Load environment variables
load_dotenv()
GATEWAY_URL = os.getenv('GATEWAY_URL')
RABBITMQ_URL = os.getenv('RABBITMQ_URL')


class TerminalSystem:
    def __init__(self, terminal_id, url):
        self.terminal_id = terminal_id
        self.redis_conn = None
        self.websocket_client = WebSocketClient(url)
        self.retry_attempts = 0
        self.max_retries = 5

    # async def __aenter__(self):
    #     await self.websocket_init()
    #     return self

    # async def __aexit__(self, exc_type, exc, tb): 
    #     await self.close_connections()


    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_   
    async def websocket_init(self):
            while self.retry_attempts < self.max_retries:
                try:
                    connected = await self.websocket_client.connect()
                    if connected:
                        print(f"Connected to the gateway. TERMINAL ID: {self.terminal_id}")
                        return
                except Exception as e:
                    print(f"Connection attempt {self.retry_attempts + 1} failed: {e}")
                self.retry_attempts += 1
                await asyncio.sleep(self.retry_attempts * 2)  # Backoff strategy
            print("Failed to connect to the gateway after several attempts.")

    async def redis_init(self):
        self.redis_conn = await redis_connection.get_connection()

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def close_connections(self):
        if self.websocket_client and self.websocket_client.connection:
            try:
                await self.websocket_client.connection.close()
            except Exception as e:
                logging.error(f"Error closing websocket connection: {e}")
            self.websocket_client.connection = None

        if self.redis_conn:
            await self.redis_conn.aclose()
            self.redis_conn = None

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_  
    async def ws_reconnect(self):
        while True:
            if not self.websocket_client.connection:
                await self.websocket_client.connect() # Try to reconnect if the connection is lost
            await asyncio.sleep(10)  # Check connection status every 10 seconds

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def run_terminal_workflow(self):
        if await self.check_terminal_registration():
            await self.send_periodic_ping()
            await self.terminal_endpoint()
        else:
            await self.register_terminal()
            await self.send_periodic_ping()
            await self.terminal_endpoint()
        # print("TERMINAL registration failed. Going to standby mode.")

    # # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def terminal_endpoint(self):
        try:
            print("Listening for messages...")

            while True:
                response = await self.websocket_client.receive_message()
                if response:
                    response = json.loads(response)
                    # print(f"Received message: {response}")  # Log every message received
                    if response.get("type") == "transaction_to_term":
                        await self.process_transaction(response.get("data"))
                        
                        # Send confirmation back to Gateway
                        confirmation_message = json.dumps({
                            "type": "confirmation",
                            "status": "processed",
                            "terminal_id": self.terminal_id  # Assuming terminal has a unique ID
                        })
                        await self.websocket_client.send_message(confirmation_message)
                    else:
                        logging.error(f"Unknown message type: {response.get('type')}")

                await asyncio.sleep(0.01)
        except websockets.exceptions.ConnectionClosedError:
            logging.error("WebSocket connection closed.")
        except Exception as e:
            logging.error(f"Error in terminal endpoint: {e}")


    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def send_periodic_ping(self):
            PING_INTERVAL = 10  # Time between pings in seconds
            while True:
                try:
                    await self.websocket_client.send_message(json.dumps({'type': 'ping', 'terminal_id': self.terminal_id}))
                    await asyncio.sleep(PING_INTERVAL)  # PING_INTERVAL is the time between pings
                except Exception as e:
                    # Handle exceptions (like connection errors)
                    print(f"Pinging failed: {e}")
                    break   

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def check_terminal_registration(self):
        await self.websocket_client.send_message(json.dumps({"type": "check_terminal_registration"}))

        response = await self.websocket_client.receive_message()
        if response:
            response = json.loads(response)
            if response.get('status') == 'success':
                logging.info("Terminal already registered.")
                return True
            else:
                if response.get('status') == 'failed':
                    logging.info("Terminal not registered. Registering...")
                    if await self.register_terminal():
                        logging.info("Terminal registration successful.")
                        return True
                    else:
                        logging.error("Terminal registration failed.")
                        return False
        return False
    
    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def register_terminal(self):
        await self.websocket_client.send_message(json.dumps({"type": "register_terminal", "terminal_id": self.terminal_id}))
        response = await self.websocket_client.receive_message()
        if response:
            response = json.loads(response)
            if response.get('status') == 'success':
                return True
            else:
                if response.get('status') == 'failed':
                    return False
        return False
    
    # # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def update_terminal_status_in_redis(self, status: str):
        try:
            msg = json.dumps({"type": "terminal_status", "terminal_id": self.terminal_id, "status": status})
            await self.websocket_client.send_message(msg)
            response = await self.websocket_client.receive_message()
            if response:
                response = json.loads(response)
                if response.get('status') == 'success':
                    logging.info(f"Terminal status updated to '{status}'")
                    return True
                else:
                    logging.error(f"Failed to update terminal status to '{status}'")
                    return False   
            return False
        except Exception as e:
            logging.error(f"Error updating terminal status: {e}")

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def listen_for_transactions(self):
        while True:
            try:
                message = await self.websocket_client.receive_message()
                if message:
                    data = json.loads(message)
                    if data.get('type') == 'transactions':
                        transactions = data.get('data')
                        # print(f"Received transaction: {transactions}")
                        await self.process_transaction(transactions)
                await asyncio.sleep(0.01)
            except Exception as e:
                print(f"Error while receiving transaction: {e}")
            await asyncio.sleep(1)  # Pause to avoid continuous loop without delay

     # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def process_transaction(self, transactions):
        print(f"Processing transaction: {transactions}")

        # Simulate some processing time
        await asyncio.sleep(random.randint(1, 3))

        if len(transactions) >0 :
            for transaction in transactions: # Simulate processing for each transaction in the list
                # processing_success = random.choice([1, 0])    
                # transaction['processed'] = processing_success
                # transaction['processed_time'] = dt.now().replace(microsecond=0).isoformat()
                
                # Send confirmation back to EPOS or update in Redis, etc.
                try:
                    msg = json.dumps({"type": "transaction_response", "terminal_id": self.terminal_id, 
                                      "result": "success", "order_id": transaction['order_id'], "cid": transaction['cid']})
                    await self.websocket_client.send_message(msg)   
                    print(f"Transaction {transaction['order_id']} processed successfully.")
                except Exception as e:
                    logging.error(f"Error processing result: {e}") 
        else:
            print(f"Transaction failed to process.")
            msg = json.dumps({"type": "transaction_response","result": "failed"})
            await self.websocket_client.send_message(msg)  
            # Handle failed transaction case
        # Send updated transaction back to EPOS
        # await self.websocket_client.send_message(json.dumps({'type': 'transaction_update', 'data': transaction}))