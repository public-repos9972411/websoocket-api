# import logging
# import os
# from dotenv import load_dotenv
# from fastapi.openapi.utils import get_openapi
# from fastapi import FastAPI
# # from gateway.core.setting import settings
# # from gateway.db.database import check_connection
# # from gateway.core.indices import create_indices
# from contextlib import asynccontextmanager
# from common.utils_server import ConnectionManager

# load_dotenv()

# TITLE = os.getenv('TITLE')
# DESCRIPTION = os.getenv('DESCRIPTION')
# VERSION = os.getenv('VERSION')
# LOCAL_URL = os.getenv('LOCAL_URL')
# LOCAL_DESCRIPTION = os.getenv('LOCAL_DESCRIPTION')


# app = FastAPI()
# manager = ConnectionManager()


# def create_app() -> FastAPI:
#     app = FastAPI(
#         title=TITLE,
#         description=DESCRIPTION,
#         version=VERSION,
#         servers=[{"url": LOCAL_URL, "description":LOCAL_DESCRIPTION}],
#         lifespan=app_lifespan
#     )
#     return app

# async def main():
#     try:
#         # Initialize database and other setup tasks
#         await init_db()
#         # Reset the state of terminals and EPOS systems
#         # await gateway_system.reset_terminal_and_epos_state()

#         await asyncio.gather(
#             gateway_system.establish_connections(),
#             # gateway_system.check_terminal_status(),
#             # gateway_system.listen_for_orders()
#             )

#         # Start the FastAPI application
#         config = uvicorn.Config(app, host="0.0.0.0", port=8000, log_level="info")
#         server = uvicorn.Server(config)
#         await server.serve()

#     except KeyboardInterrupt:
#         print("Keyboard Interrupt. Exiting...")
#         # await manager.close_all_connections()

#     except asyncio.CancelledError:
#         print("Application interrupted. Cleaning up...")
#         # await gateway_system.close_connections()
#         # await manager.close_all_connections()

#     except Exception as e:
#         print(f"Unexpected error: {e}")
#         # await gateway_system.close_connections()
#         # await manager.close_all_connections()

# @asynccontextmanager
# async def app_lifespan(app: FastAPI):
#     # Startup actions 
#     print("statring up...")
#     await main()

#     yield
#     # Shutdown actions

#     print("Application is shutting down. Performing cleanup...")
#     await manager.close_all_connections()
#     print("Shutdown complete.")
