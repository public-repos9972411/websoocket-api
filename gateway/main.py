# gateway/app/main.py

import asyncio, sys, uvicorn, os, logging
from fastapi import FastAPI,WebSocket, WebSocketDisconnect
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
from db.db_setup import client
from db import db_setup
from utils import GatewaySystem
from common.utils_server import WebSocketServer
# from starlette.websockets import WebSocketDisconnect
# from contextlib import asynccontextmanager


app = FastAPI()

manager = WebSocketServer()
gateway_system = GatewaySystem(manager)

# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
@app.websocket("/ws/epos/{epos_id}")
async def epos_endpoint(websocket: WebSocket, epos_id: str):
    await gateway_system.epos_endpoint(websocket, epos_id)
    try:
       pass
    except WebSocketDisconnect:
        await gateway_system.manager.disconnect(epos_id)

# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
@app.websocket("/ws/terminal/{terminal_id}")
async def terminal_endpoint(websocket: WebSocket, terminal_id: int):
    await gateway_system.terminal_endpoint(websocket, terminal_id)
    try:
       pass
    except WebSocketDisconnect:
        await gateway_system.manager.disconnect(terminal_id)

# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
async def init_db():
    if not await db_setup.is_initialization_done():
        await db_setup.create_indices()
        await db_setup.mark_initialization_done()
        print("Database initialization completed.")
    else:
        print("Database already initialized.")

# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
async def main():

    try:
        await init_db()
        await gateway_system.async_init()

        print("Starting GATEWAY application...")
        check_status_task = asyncio.create_task(gateway_system.check_terminal_status())

        # Start the FastAPI application
        config = uvicorn.Config(app, host="0.0.0.0", port=8000, log_level="info")
        server = uvicorn.Server(config)
        await server.serve()

    except KeyboardInterrupt:
        print("Keyboard Interrupt. Exiting...")
        await manager.close_all_connections()
    except asyncio.exceptions.CancelledError:
        print("GATEWAY application interrupted. Cleaning up...")
        await manager.close_all_connections()
    finally:
        print("Closing connections and cleaning up resources...")
        check_status_task.cancel()
        try:
            await check_status_task
        except asyncio.CancelledError:
            pass
        await gateway_system.close_connections()
        if client:
            client.close()
        print("GATEWAY application shutdown.")


if __name__ == "__main__":
    asyncio.run(main())

    
