import asyncio, os, sys, logging, json
from typing import Dict, List, Optional, Tuple
from dotenv import load_dotenv
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
from db.redis import redis_connection
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
from fastapi import WebSocket
from starlette.websockets import WebSocketDisconnect
from datetime import datetime as dt
from redis.exceptions import RedisError
from common.utils_server import WebSocketServer

# Load environment variables
load_dotenv()
GATEWAY_URL = os.getenv('GATEWAY_URL')
RABBITMQ_URL = os.getenv('RABBITMQ_URL')

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class GatewaySystem:
    def __init__(self, manager):
        self.manager = manager
        self.redis_conn = None
        self.server_conn = WebSocketServer()
   
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_    
    async def async_init(self):
        self.redis_conn = await redis_connection.get_connection()

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def close_connections(self):
        if self.server_conn and self.server_conn.connect:
            try:
                await self.server_conn.close_all_connections()
            except Exception as e:
                logging.error(f"Error closing WebSocket connection: {e}")
            self.server_conn.connect = None

        if self.redis_conn:
            await self.redis_conn.aclose()
        self.redis_conn = None

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def terminal_endpoint(self, websocket: WebSocket, terminal_id: int):
        await self.manager.connect(websocket, terminal_id)
        try:
            while True:
                data = await websocket.receive_text()
                if data is not None:
                    data = json.loads(data)
    
                    if data.get("type") == "check_terminal_registration":
                        print("------inside check_terminal_registration------")
                        if await self.check_terminal_registration(terminal_id):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "register_terminal":
                        print("------inside register_terminal------")
                        await self.redis_conn.hset(f"terminal:{terminal_id}", "registered", "True")
                        if await self.redis_conn.sadd("registered_terminals", terminal_id):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "terminal_status":
                        print("------inside terminal_status------")
                        if await self.update_terminal_status(data.get("terminal_id"), data.get("status")):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "ping":
                        print("received ping from terminal")
                        # Update last seen time
                        await self.manager.handle_ping(terminal_id, dt.now().replace(microsecond=0).isoformat())

                    elif data.get("type") == "get_terminals":
                        print("fetching terminal list from redis")
                        available_terminals_for_customer = await self.get_available_terminals_for_customer(customer_id=data.get("customer_id"))
                        # broadcast to selected EPOS systems
                        await self.broadcast_to_epos_systems(self, available_terminals_for_customer)
                        
                        # send the list to EPOS system
                        prepared_data = json.dumps({"type": "terminal_list", "terminal_list": available_terminals_for_customer})
                        await websocket.send_text(prepared_data)

                    elif data.get("type") == "transaction_response":
                        
                        if data.get("result") == "failed":
                            return
                        terminal_id = data.get("terminal_id")
                        # status = data.get("status")
                        order_id = data.get("order_id")
                        # serial_id = data.get("serial_id")
                        cid = data.get("cid")
                        print(f"Received transaction response from terminal {terminal_id}: {cid}")

                        # fetch transaction from redis
                        transaction_data = await self.redis_conn.hgetall(f"transaction:{order_id}")
                        # update the processed field to 1
                        transaction_data['processed'] = 1
                        # update the processed_time field to the current time
                        # transaction_data['processed_time'] = dt.now().replace(microsecond=0).isoformat()

                        # store the transaction in redis
                        await self.redis_conn.hset(f"transaction:{order_id}", mapping=transaction_data)
                        print("Processed: ", transaction_data)

                        # broadcast to selected EPOS systems
                        prepared_data = json.dumps({"type": "transaction_update", "data": transaction_data})
                        await self.broadcast_to_epos_systems(cid, prepared_data)
                        # await self.manager.send_personal_message(prepared_data, serial_id)
                        # available_terminals_for_customer = await self.get_available_terminals_for_customer(customer_id=transaction_data.get("cid"))
                        print("broadcasting to epos systems")
                        # await self.broadcast_to_epos_systems(self, available_terminals_for_customer)
                        # send the list to EPOS system
                        # prepared_data = json.dumps({"type": "transaction_update", "transaction": transaction_data})
                        # await websocket.send_text(prepared_data)

                    # elif data.get("type") == "transaction_update":
                    #     print("transaction update from terminal")
                await asyncio.sleep(0.01) 
        except WebSocketDisconnect:
            await self.manager.disconnect(terminal_id)
        except Exception as e:
            print(f"Error in terminal_endpoint: {e}")
            await self.manager.disconnect(terminal_id)
            raise e
    
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def epos_endpoint(self, websocket: WebSocket, serial_id: str):
        await self.manager.connect(websocket, serial_id)
        try:
            # get the customer id for the given serial id
            customer_id = await self.fetch_customer_id_for_epos(serial_id)

            while True:
                data = await websocket.receive_text()

                if data is not None:
                    data = json.loads(data)
                    # print(data)
                    if data.get("type") == "check_epos_registration":
                        if await self.check_epos_registration(serial_id):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "register_epos":
                        await self.redis_conn.hset(f"epos:{serial_id}", "registered", "True")
                        if await self.redis_conn.sadd("registered_epos", serial_id):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "unupdated_transactions":
                        print("------inside_unupdated_transactions------")
                        # fetch unupdated transactions from redis
                        _, unupdated_transactions = await self.fetch_unupdated_transactions_by_customer(customer_id)
                        prepared_data = json.dumps({"type": "unupdated_transactions_list", "unupdated_transactions_list": unupdated_transactions})

                        if len(unupdated_transactions) > 0:
                            await self.update_retrieved_transactions(unupdated_transactions, websocket)
                            # broadcast to selected EPOS systems
                            await self.broadcast_to_epos_systems(customer_id, prepared_data)
                        else:
                            await websocket.send_text(json.dumps({"status": "no transactions to update"}))

                    # elif data.get("type") == "update_retrieved":
                    #     print("------update_retrieved------")
                    #     # get the transactions to update
                    #     trnxs = data.get("transactions")
                    #     print("trnxs: ", trnxs)
                    #     await self.update_retrieved_transactions(trnxs, websocket)
                        
                    elif data.get("type") == "get_terminals":
                        print("------inside_get_epos_get_terminals------")
                        available_terminals_for_customer = await self.get_available_terminals_for_customer(customer_id)
                        prepared_data = json.dumps({"type": "terminal_list", "terminal_list": available_terminals_for_customer})
                        # broadcast to selected EPOS systems
                        await self.broadcast_to_epos_systems(customer_id, prepared_data)

                    elif data.get("type") == "transactions":
                        print("------inside_transactions--gateway----")
                        try:
                            # send transactions to terminal
                            terminalid = int(data.get("data")[0].get("terminal_id"))
                            print("sending transactions to terminal", terminalid)

                            msg = json.dumps({"type": "transaction_to_term", "data": data.get("data")})
                            # print("msg: ", msg)
                            await self.manager.send_personal_message(msg, terminalid)
                            # await websocket.send_text(json.dumps({"type": "transactions_list", "msg": msg}))

                            # store transactions in redis
                            print("storing transactions in redis")
                            if (await self.store_transactions(data.get("data"))):
                                await websocket.send_text(json.dumps({"status": "success"}))
                            else:
                                await websocket.send_text(json.dumps({"status": "failed"}))
                        except Exception as e:
                            print(f"Error sending transactions: {e}")
                            await websocket.send_text(json.dumps({"status": "failed"}))

                    elif data.get("type") == "terminal_status":
                        print("------inside_epos_termianl_status------")
                        if await self.update_terminal_status(data.get("terminal_id"), data.get("status")):
                            await websocket.send_text(json.dumps({"status": "success"}))
                        else:
                            await websocket.send_text(json.dumps({"status": "failed"}))
                    elif data.get("type") == "confirmation":
                        terminal_id = data.get("terminal_id")
                        status = data.get("status")
                        print(f"Received confirmation from terminal {terminal_id}: {status}")
                        # # fetch unupdated transactions from redis
                        # unupdated_transactions, _ = await self.fetch_unupdated_transactions_by_customer(customer_id)
                        # prepared_data = json.dumps({"type": "unupdated_transactions_list", "unupdated_transactions_list": unupdated_transactions})
                        #  # broadcast to selected EPOS systems
                        # await self.broadcast_to_epos_systems(customer_id, prepared_data)
                    else:
                        print("------inside_else------")
                        await websocket.send_text(json.dumps({"status": "failed"}))

        except WebSocketDisconnect:
            await self.manager.disconnect(serial_id)
        except Exception as e:
            print(f"Error in epos_endpoint: {e}")
            await self.manager.disconnect(serial_id)
            raise e
    


    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    # async def check_epos_registration(self, serial_id: str):
    #     registered = await self.redis_conn.hget(f"epos:{serial_id}", "registered")
    #     return registered == "True"
    
        # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def check_epos_registration(self, serial_id: str) -> bool:
        if not self.redis_conn:
            self.redis_conn = await redis_connection.get_connection()
        return await self.redis_conn.sismember("registered_epos", serial_id)

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def fetch_customer_id_for_epos(self, serial_id):
        # Fetching the customer id for a given epos
        customer_id = await self.redis_conn.hget(f"epos:{serial_id}", "cid")
        return customer_id

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def check_terminal_registration(self, terminal_id: int) -> bool:
        if not self.redis_conn:
            self.redis_conn = await redis_connection.get_connection()
        return await self.redis_conn.sismember("registered_terminals", terminal_id)
    
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def get_available_epos_for_customer(self, customer_id: str):
        available_epos_for_customer = []
        # Fetch all available epos
        if self.redis_conn:
            available_epos = await self.redis_conn.smembers('available_epos')
            # print("--------inside-get-available-terminals-for-customer--------")
            # Check each serial's customer ID
            for serial_id in available_epos:
                epos_customer_id = await self.redis_conn.hget(f"epos:{serial_id}", "cid")
                if epos_customer_id == customer_id:
                    available_epos_for_customer.append(serial_id)
        return available_epos_for_customer
    
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def get_available_terminals_for_customer(self, customer_id: str):
        available_terminals_for_customer = []
        # Fetch all available terminals
        if self.redis_conn:
            available_terminals = await self.redis_conn.smembers('available_terminals')
            # print("--------inside-get-available-terminals-for-customer--------")
            # Check each terminal's customer ID
            # print("available_terminals: ", available_terminals)
            for terminal_id in available_terminals:
                terminal_customer_id = await self.redis_conn.hget(f"terminal:{terminal_id}", "cid")
                if terminal_customer_id == customer_id:
                    available_terminals_for_customer.append(terminal_id)
        return available_terminals_for_customer
    
    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def fetch_unupdated_transactions_by_customer(self, customer_id: str, page_size=10)-> Tuple[int, List[dict]]:
        try:
            cursor = '0'
            unupdated_transactions = []
            if not self.redis_conn:
                self.redis_conn = await redis_connection.get_connection()
            while cursor != 0:
                cursor, keys = await self.redis_conn.scan(cursor=cursor, match="transaction:*", count=page_size)
                
                for key in keys:
                    transaction_data = await self.redis_conn.hgetall(key) 
                    if transaction_data.get('retrieved') == '0'  and transaction_data.get('cid') == customer_id:  
                        unupdated_transactions.append(transaction_data)
            return cursor, unupdated_transactions
        except Exception as e:
            print(f"Error fetching unupdated transactions: {e}")
            # return 0, []


    async def get_customerid_from_serialid(self, serial_id):
        return await self.redis_conn.hget(f"epos:{serial_id}", "cid")
    

    async def create_customer_epos_set(self, customer_id, serial_id):
        return await self.redis_conn.sadd(f"customer_epos:{customer_id}", serial_id)

    async def register_epos_system(self, serial_id):
        return await self.redis_conn.hset(f"epos:{serial_id}", "registered", "True")
        

    # fetch all the serial IDs from the Redis set corresponding to the customer ID.
    async def get_epos_systems_for_customer(self, customer_id):
        if not self.redis_conn:
            self.redis_conn = await redis_connection.get_connection()  
        epos_set_key = f"customer_epos:{customer_id}"
        epos_systems_set = await self.redis_conn.smembers(epos_set_key)
        return list(epos_systems_set)

    
    async def broadcast_to_epos_systems(self, customer_id, prepared_data):
        serial_ids = await self.get_epos_systems_for_customer(customer_id)
        print("serial_ids: ", serial_ids)
        # broadcast to selected EPOS systems
        for sid in serial_ids:
            await self.manager.send_personal_message(prepared_data, sid)


    async def check_terminal_status(self):
        OFFLINE_THRESHOLD = 15 # seconds
        CHECK_INTERVAL = 10  # seconds
        try:
            print("------------inside---new---check-----------")
            if not self.redis_conn:
                    self.redis_conn = await redis_connection.get_connection()            
            while True:
                registered_terminals = await self.get_registered_terminals()
                print("registered terminals: ", registered_terminals)
                if len(registered_terminals) > 0:
                    for terminal_id in registered_terminals:
                        last_seen_str = await self.redis_conn.hget(f"terminal:{terminal_id}", "last_seen")
                        if last_seen_str:
                            last_seen = dt.fromisoformat(last_seen_str)
                            if (dt.utcnow() - last_seen).total_seconds() > OFFLINE_THRESHOLD:
                                await self.update_terminal_status(terminal_id, "Offline")
                            else:
                                await self.update_terminal_status(terminal_id, "Online")

                await asyncio.sleep(CHECK_INTERVAL)
        except Exception as e:
            logging.error(f"Error checking terminal status: {e}")



    def is_offline(self, last_seen, threshold):
        return (dt.now() - (dt.fromisoformat(last_seen))).total_seconds() > threshold  

     # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def update_terminal_info(terminal_id: int, status: str = None, last_seen: str = None):
        try:
            print("------------inside---update-terminal----------")
            redis_conn = await redis_connection.get_connection()
            if status:
                await redis_conn.hset(f"terminal:{terminal_id}", "status", status)
            if last_seen:
                await redis_conn.hset(f"terminal:{terminal_id}", "last_seen", last_seen)        
        except Exception as e:
            print(f"Error updating terminal info: {e}")


    async def get_registered_terminals(self):
        return list(await self.redis_conn.smembers("registered_terminals"))

    async def update_terminal_status(self, terminal_id, status, last_seen=None) -> bool:
        try:
            key = f"terminal:{terminal_id}"
            # Initialize a pipeline transaction
            trx = self.redis_conn.pipeline()
            # Update status and last seen time
            trx.hset(key, "status", status)
            if last_seen:
                trx.hset(key, "last_seen", last_seen)

            # Update the set of available terminals based on status
            if status == "Offline":
                trx.srem("available_terminals", terminal_id)
            else:
                # print("------av---------")
                trx.sadd("available_terminals", terminal_id)

            # Execute all commands in the pipeline
            if await trx.execute():
                logging.info(f"Updated status for terminal {terminal_id} to {status}")
                return True
            else:
                logging.error(f"Failed to update status for terminal {terminal_id} to {status}")
                return False
        
        except RedisError as e:
            logging.error(f"Error updating terminal status for {terminal_id}: {e}")
            await self.handle_exception(terminal_id)
            return False


    async def handle_exception(self, terminal_id):
        logging.error(f"Handling exception for terminal {terminal_id}")
        try:
            await self.redis_conn.srem("available_terminals", terminal_id)
        except RedisError as e:
            logging.error(f"Error during exception handling for {terminal_id}: {e}")


    async def store_transactions(self, transactions):
        count = 0
        for transaction in transactions:
            try:
                # Serialize only the 'items' list as a JSON string
                transaction['items'] = json.dumps(transaction['items'])

                # Convert boolean values to integers (0 or 1)
                transaction['processed'] = int(transaction['processed'])
                transaction['retrieved'] = int(transaction['retrieved'])

                # Store transaction in Redis
                transaction_key = f"transaction:{transaction['order_id']}"
                await self.redis_conn.hset(transaction_key, mapping=transaction)
                count+=1
            except Exception as e:
                logging.error(f"Error storing transaction: {transaction}. Error: {e}")
        logging.info(f"Stored {count} transactions to redis")

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
    async def update_transaction_to_processed(self, customer_id: str, page_size=10)-> Tuple[int, List[dict]]:
        try:
            cursor = '0'
            unupdated_transactions = []
            if not self.redis_conn:
                self.redis_conn = await redis_connection.get_connection()
            while cursor != 0:
                cursor, keys = await self.redis_conn.scan(cursor=cursor, match="transaction:*", count=page_size)
                
                for key in keys:
                    transaction_data = await self.redis_conn.hgetall(key) 
                    if transaction_data.get('processed') == '0'  and transaction_data.get('cid') == customer_id:  
                        unupdated_transactions.append(transaction_data)
            return cursor, unupdated_transactions
        except Exception as e:
            print(f"Error fetching unupdated transactions: {e}")


    async  def update_retrieved_transactions(self, unupdated_transactions, websocket: WebSocket):
        # update the retrieved field to 1
        # if unupdated_transactions:
        if not self.redis_conn:
            self.redis_conn = await redis_connection.get_connection()

        # get all order_ids in the list of transactions
        order_ids = [trnx.get("order_id") for trnx in unupdated_transactions]

        for order_id in order_ids:
            await self.redis_conn.hset(f"transaction:{order_id}", "retrieved", 1)
        await websocket.send_text(json.dumps({"type": "retrivced_updated", "status": "success"}))
        # else:
        #     await websocket.send_text(json.dumps({"status": "empty"}))



