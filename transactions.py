
import asyncio
import json
import logging
from typing import List, Tuple
from db.redis import redis_connection

transactions = [
    {
        "order_id": 1001,
        "items": ["item1", "item2", "item3"],
        "amount": 45.50,
        "serial_id": "8899",
        "type": "sale",
        "terminal_id": 8010,
        "timestamp": "2023-11-24T12:00:00",
        "processed": False,
        "retrieved": False,
        "cid": "3155"
    },
    {
        "order_id": 1002,
        "items": ["item4", "item5"],
        "amount": 30.00,
        "serial_id": "8899",
        "type": "sale",
        "terminal_id": 8011,
        "timestamp": "2023-11-24T12:05:00",
        "processed": False,
        "retrieved": False,
        "cid": "3155"
    }
]


async def store_transactions(transactions):
    redis_conn = await redis_connection.get_connection()
    for transaction in transactions:
        try:
            # Serialize only the 'items' list as a JSON string
            transaction['items'] = json.dumps(transaction['items'])

            # Convert boolean values to integers (0 or 1)
            transaction['processed'] = int(transaction['processed'])
            transaction['retrieved'] = int(transaction['retrieved'])

            # Store transaction in Redis
            transaction_key = f"transaction:{transaction['order_id']}"
            await redis_conn.hset(transaction_key, mapping=transaction)
            logging.info(f"Stored transaction: {transaction_key}")

        except Exception as e:
            logging.error(f"Error storing transaction: {transaction}. Error: {e}")
    
    await redis_conn.aclose()


async def fetch_unupdated_transactions_by_customer(customer_id: str, page_size=10)-> Tuple[int, List[dict]]:
    redis_conn = await redis_connection.get_connection()

    try:
        cursor = '0'
        unupdated_transactions = []

        while cursor != 0:
            cursor, keys = await redis_conn.scan(cursor=cursor, match="transaction:*", count=10)
            
            for key in keys:
                transaction_data = await redis_conn.hgetall(key) 
                if transaction_data.get('retrieved') == '0'  and transaction_data.get('cid') == customer_id:  
                    unupdated_transactions.append(transaction_data)
        return cursor, unupdated_transactions

    finally:
        await redis_conn.aclose()


async def main():
    curr, unupdated_transactions = await fetch_unupdated_transactions_by_customer("3155")
    print(unupdated_transactions)


if __name__ == "__main__":
    asyncio.run(main())

