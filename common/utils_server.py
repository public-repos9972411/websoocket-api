# gateway/app/connections/websocket_manager.py

import asyncio
import json
import logging
import sys
from typing import Dict, Union
from fastapi import WebSocket
from fastapi.websockets import WebSocketState
from redis import RedisError
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
from db.redis import redis_connection


class WebSocketServer:
    def __init__(self):
        self.active_connections: Dict[int, WebSocket] = {}
        self.redis_client = None
        # self.server_conn = None # The actual WebSocket connection

    # # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def establish_redis_connection(self):
        try:
            self.redis_client = await redis_connection.get_connection()
        except Exception as e:
            logging.error(f"Failed to establish Redis connection: {e}")

    # # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def connect(self, websocket: WebSocket, client_id: Union[int, str]):
        await websocket.accept()
        self.active_connections[client_id] = websocket
        terminal_ids = list(self.active_connections.keys()) 
        print(f"active connections: {terminal_ids}")
        print(f"Client {client_id} connected")
        logging.info("Connection successful")
        # print(f'active connections: {self.active_connections}')

    async def disconnect(self, client_id: Union[int, str]):
        if client_id in self.active_connections:
            websocket = self.active_connections[client_id]
            if not websocket.client_state == WebSocketState.DISCONNECTED:
                await websocket.close()
            del self.active_connections[client_id]
            print(f"WebSocket disconnected from {client_id}")
        else:
            print(f"WebSocket not found for {client_id}")

    async def send_personal_message(self, message: str, client_id: Union[int, str]):
        if client_id in self.active_connections:
            await self.active_connections[client_id].send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections.values():
            await connection.send_text(message)

    async def close_all_connections(self):
        for client_id, websocket in self.active_connections.items():
            if websocket and websocket.client_state != WebSocketState.DISCONNECTED:
                message = "Server shutting down"
                await websocket.send_text(message)
                await websocket.close()
            print(f"Closed connection for {client_id}")
        self.active_connections.clear()
        if self.redis_client:
            await self.redis_client.aclose()

    async def handle_ping(self, client_id, timestamp):
        if not self.redis_client:
            # If for some reason the connection is not established, re-establish it
            await self.establish_redis_connection()
        try:
            # Use the Redis client to perform the hset operation
            await self.redis_client.hset(f"terminal:{client_id}", mapping={"last_seen": timestamp, "status": "Available"})
        except RedisError as e:
            logging.error(f"Redis error: {e}")


