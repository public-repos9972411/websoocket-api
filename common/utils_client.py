import uuid
import json
import asyncio
import websockets
import logging
from bson import ObjectId


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
    
def generate_unique_id():
    return str(uuid.uuid4()) 


class WebSocketClient:
    def __init__(self, url):
        self.url = url
        self.connection = None # The actual WebSocket connection
        self.retry_count = 0
        self.max_retries = 5
        self.retry_delay = 5  # seconds

    async def connect(self):
        while self.retry_count < self.max_retries:
            try:
                logging.info(f"Attempt {self.retry_count + 1} to connect...")
                
                try:
                    self.connection = await websockets.connect(self.url)
                    logging.info("WebSocket connection successful.")
                    self.retry_count = 0  # Reset retry count after successful connection
                    return True
                except asyncio.TimeoutError:
                    logging.error("WebSocket connection failed: Timeout error.")
                    self.retry_count += 1
                    await asyncio.sleep(self.retry_delay)
                # else:
                #     logging.error("WebSocket connection failed.")
                #     return False
            except (websockets.ConnectionClosedError, websockets.InvalidHandshake, OSError) as e:
                logging.error(f"WebSocket connection failed: {e}. Retrying...")
                self.retry_count += 1
                await asyncio.sleep(self.retry_delay)
        
        logging.error("Failed to connect to WebSocket. Max retries reached.")
        self.connection = None
        return False
    
    async def send_message(self, message):
        if self.connection:
            try:
                await self.connection.send(message)
            except websockets.exceptions.WebSocketException as e:
                logging.error(f"Error sending message: {e}")
                self.connection = None  # Reset connection on error
                return None        

    async def receive_message(self):
        if self.connection:
            try:
                return await self.connection.recv() 
            except websockets.exceptions.WebSocketException as e:
                logging.error(f"Error receiving message: {e}")
                self.connection = None  # Reset connection on error
                return None
               
    async def close_connections(self):
        if self.connection and not self.connection.closed:
            try:
                if self.connection:
                    await self.connection.close()
            except websockets.exceptions.WebSocketException as e:
                logging.error(f"Error closing WebSocket connection: {e}")
                return None
            finally:
                self.connection = None
                logging.info("WebSocket connection closed.")
                return None

    async def wait_for_disconnection(self):
        if self.connection and not self.connection.closed:
            try:
                # This will block until a message is received or an error occurs (such as disconnection)
                await self.connection.recv()
            except websockets.exceptions.WebSocketException as e:
                logging.error(f"WebSocket disconnected: {e}")
                self.connection = None
                raise