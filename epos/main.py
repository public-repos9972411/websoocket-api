# Description: This file contains the main EPOS application code.
import asyncio
import sys
from utils import EposSystem

# +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_ 
async def main(serial_id):
    url = f"ws://localhost:8000/ws/epos/{serial_id}"
    epos_system = EposSystem(serial_id, url)
    monitor_task = None

    try:
        print("Starting EPOS application...")
        await epos_system.websocket_init()
        await epos_system.redis_init()
        monitor_task = asyncio.create_task(epos_system.ws_reconnect())

        if not await epos_system.check_epos_registration():
            return
        
        await epos_system.handle_unupdated_transactions()
        await epos_system.get_available_terminals()
        await asyncio.sleep(5)
        await epos_system.send_transactions()
        await epos_system.listen_to_gateway_messages()


    except KeyboardInterrupt:
        print("Keyboard Interrupt. Exiting...")
        await epos_system.close_connections()
    except asyncio.exceptions.CancelledError:
        print("EPOS application interrupted. Cleaning up...")
        await epos_system.close_connections()
    finally:
        if monitor_task:
            monitor_task.cancel()
        await epos_system.close_connections()
        print("EPOS application shutdown.")


if __name__ == "__main__":
    # serial_id = "8899"
    # serial_id = "8900"
    if len(sys.argv) < 2:
        print("Usage: python terminal.py <serial_id>")
        sys.exit(1)
    try:
        serial_id = str(sys.argv[1])
    except ValueError:
        print("Invalid serial_id. Please enter a valid string.")
        sys.exit(1)

    asyncio.run(main(serial_id))
