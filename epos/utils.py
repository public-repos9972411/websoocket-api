import asyncio, logging, json, sys, os, websockets, random
from datetime import datetime as dt
# from typing import Optional
from dotenv import load_dotenv
sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
# from db.db_setup import Epos
from db.redis import redis_connection
from common.utils_client import WebSocketClient


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Load environment variables
load_dotenv()


class EposSystem:
    def __init__(self, serial_id, url):
        self.serial_id = serial_id
        self.websocket_client = WebSocketClient(url)
        self.retry_attempts = 0
        self.max_retries = 5
        self.redis_conn = None

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_   
    async def websocket_init(self):
            while self.retry_attempts < self.max_retries:
                try:
                    connected = await self.websocket_client.connect()
                    if connected:
                        print(f"Connected to the gateway. Serial ID: {self.serial_id}")
                        return
                except Exception as e:
                    print(f"Connection attempt {self.retry_attempts + 1} failed: {e}")
                self.retry_attempts += 1
                await asyncio.sleep(self.retry_attempts * 2)  # Backoff strategy
            print("Failed to connect to the gateway after several attempts.")

    async def redis_init(self):
        self.redis_conn = await redis_connection.get_connection()

    async def close_connections(self):
        if self.websocket_client and self.websocket_client.connection:
            try:
                await self.websocket_client.connection.close()
            except Exception as e:
                logging.error(f"Error closing WebSocket connection: {e}")
            self.websocket_client.connection = None

        if self.redis_conn:
            await self.redis_conn.aclose()
        self.redis_conn = None

    # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_  
    async def ws_reconnect(self):
        while True:
            if not self.websocket_client.connection:
                await self.websocket_client.connect() # Try to reconnect if the connection is lost
            await asyncio.sleep(10)  # Check connection status every 10 seconds


    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
        


    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def listen_to_gateway_messages(self):
        print("-----running epos workflow-----")
        while True:
            try:
                message = await self.websocket_client.receive_message()
                if message:
                    data = json.loads(message)
                    # Handle different types of messages here
                    # if data.get('type') == 'transactions_list':
                    #     await self.get_available_terminals()

                    if data.get('type') == 'unupdated_transactions_list':
                        await self.handle_unupdated_transactions()

                    elif data.get('type') == 'transaction_update': 
                        print("Transaction processed successfully.")
                        
                    elif data.get('type') == 'transaction_response': # transaction response from gateway
                        await self.update_transaction()
                    
                await asyncio.sleep(0.01) 
            except Exception as e:
                logging.error(f"Error receiving message from gateway: {e}")
                await asyncio.sleep(1)

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def register_epos(self):
        await self.websocket_client.send_message(json.dumps({"type": "register_epos", "serial_id": self.serial_id}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            if response_data.get('status') == 'success':
                print("EPOS registration successful.")
                return True
            else:
                print("EPOS registration failed.")
                return False
        return False

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def check_epos_registration(self):
        await self.websocket_client.send_message(json.dumps({"type": "check_epos_registration"}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            if response_data.get('status') == 'success':
                logging.info("EPOS already registered.")
                return True
            else:
                if response_data.get('status') == 'failed':
                    logging.info("EPOS not registered. Registering...")
                    if await self.register_epos():
                        logging.info("EPOS registration successful.")
                    else:
                        logging.info("EPOS registration failed.")
                        return False
                    return False
        return False


    #  async def register_epos(epos_id, customer_id):
    #     # Use a Redis Set to store EPOS IDs for each customer
    #     await redis_client.sadd(f"customer:{customer_id}:epos", epos_id)

    async def register_epos(self):
            await self.websocket_client.send_message(json.dumps({"type": "register_epos", "serial_id": self.serial_id}))
            response = await self.websocket_client.receive_message()
            if response:
                response = json.loads(response)
                if response.get('status') == 'success':
                    return True
                else:
                    if response.get('status') == 'failed':
                        return False
            return False

     # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def update_transaction(self) -> bool:
        await self.websocket_client.send_message(json.dumps({"type": "update_transaction"}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            return response_data.get('status') == 'success'

    async def show_update(self) -> bool:
        # await self.websocket_client.send_message(json.dumps({"type": "update_transaction"}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            return response_data.get('status') == 'success'
        

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def handle_unupdated_transactions(self):
        await self.websocket_client.send_message(json.dumps({"type": "unupdated_transactions"}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            if response_data.get("type") == "unupdated_transactions_list":
                unupdated_transactions = response_data.get("unupdated_transactions_list")
                print("Updated transactions:", unupdated_transactions)

    
    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def update_retrieved(self, unupdated_transactions):
        await self.websocket_client.send_message(json.dumps({'type': 'update_retrieved', 'serial_id': self.serial_id, 'transactions': unupdated_transactions}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            if response_data.get("type") == "success":
                print("Transactions updated successfully.")
            else:
                if response_data.get("type") == "empty":
                    print("No Transactions to update")
                else:
                    print("Failed to update transactions")
        return False
    
    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def update_transactions(self, transactions):
        if not transactions:
            logging.info("EPOS has no transactions to be updated.")
            return False

        cid = await self.fetch_customer_id_for_epos(serial_id=self.serial_id)
        count = 0
        for transaction in transactions:
            if transaction.get('cid') == cid:
                transaction_key = f"transaction:{transaction['order_id']}"
                await self.redis_conn.hset(transaction_key, 'retrieved', 1)  # change retrieved field to 1
                count += 1
        logging.info(f"Updated {count} transactions.")
        return count > 0
     

    async def request_available_terminals(self):
        await self.websocket_client.send_message(json.dumps({'type': 'get_terminals', 'serial_id': self.serial_id}))
        response = await self.websocket_client.receive_message()
        if response:
            response_data = json.loads(response)
            if response_data.get("type") == "terminal_list":
                available_terminals = response_data.get("terminal_list")
                # print("Available Terminals:", available_terminals)
            else:
                available_terminals = []
        return False


     # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def get_available_terminals(self):
        await self.websocket_client.send_message(json.dumps({'type': 'get_terminals', 'serial_id': self.serial_id}))
        response = await self.websocket_client.receive_message()
        if response:
            response = json.loads(response)
            if response.get("type") == "terminal_list":
                available_terminals = response.get("terminal_list")
                print("Available terminals: ", available_terminals)
                return available_terminals
            else:
                return []
        return False


    async def get_available_terminals_for_customer(self, customer_id):
        terminals = []
        cursor = '0'

        while cursor != 0:
            cursor, keys = await self.redis_conn.scan(cursor=cursor, match='terminal:*',count=10)
            if not keys:
                break
            commands = [self.redis_conn.hget(key, 'cid') for key in keys]
            customer_ids = await asyncio.gather(*commands)
            
            for terminal, id in zip(keys, customer_ids):
                if id == customer_id:  
                    terminals.append(terminal)
        return terminals
    

    async def fetch_customer_id_for_epos(self, serial_id):
        # Fetching the customer id for a given terminal
        customer_id = await self.redis_conn.hget(f"epos:{serial_id}", "cid")
        return customer_id

    async def send_transactions(self):
        available_terminals = await self.get_available_terminals()
        # print(f"Available terminals: {available_terminals}")

        if not available_terminals:
            print("No terminals available at the moment")
            return
        
        cid = await self.fetch_customer_id_for_epos(serial_id=self.serial_id) 
        if not cid:
            print("No customer id found for this terminal")
            return
        print(f"Customer id for this terminal: {cid}")
        selected_terminal_id = random.choice(available_terminals)
        print(f"Selected terminal id: {selected_terminal_id}")

        transaction = [{
            "order_id": random.randint(100000, 999999), 
            "items": ["item1", "item2", "item3"],
            "amount": round(random.uniform(0, 100), 2), 
            "serial_id": "8899",
            "type": "sale",
            "terminal_id": selected_terminal_id,
            "timestamp": dt.now().replace(microsecond=0).isoformat(),
            # "processed_time": None,
            "processed": False,
            "retrieved": False,
            "cid": cid
        }]

        await self.websocket_client.send_message(json.dumps({'type': 'transactions', 'data': transaction}))
        print("Transaction sent to gateway.")
