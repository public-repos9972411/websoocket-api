# import requests

# gateway_url = "http://localhost:8000/update_terminal_status"

# def update_terminal_status(terminal_id: str, status: str):
    
#     response = requests.post(f"{gateway_url}/{terminal_id}/{status}")
#     print(response.json())

# # Example usage
# update_terminal_status("terminal1", "Available")

import asyncio
import sys
import csv


sys.path.append('/Users/lwanga/Documents/HARK/pylon/')
# from db.db_setup import Epos, Terminals
from db.redis import redis_connection


# async def main():
    
#     redis_conn = await redis_connection.get_connection()
#     await redis_conn.hset(f"terminal:{terminal_id}", "customer_id", customer_id)


# async def save_csv_to_redis(file_path):
#     redis_conn = await redis_connection.get_connection()
#     with open(file_path, mode='r', encoding='utf-8') as file:
#         csv_reader = csv.DictReader(file)
#         for row in csv_reader:
#             hash_key = f"terminal:{row['tid']}"
#             for field, value in row.items():
#                 await redis_conn.hset(hash_key, field, value)

#     await redis_conn.aclose()


async def save_csv_to_redis(file_path):
    redis_conn = await redis_connection.get_connection()
    with open(file_path, mode='r', encoding='utf-8') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            hash_key = f"epos:{row['serial_id']}"
            for field, value in row.items():
                await redis_conn.hset(hash_key, field, value)

    await redis_conn.aclose()


# async def delete_all_transactions():
#     redis_conn = await redis_connection.get_connection()
#     pattern = "transaction:*"
#     cursor = '0'
#     while cursor != 0:
#         cursor, keys = await redis_conn.scan(cursor=cursor, match=pattern, count=10)
#         for key in keys:
#             await redis_conn.delete(key)
#             print(f"Deleted key: {key}")  # Optional: Print deleted key

if __name__ == "__main__":
    # asyncio.run(main())
    file_path = 'epos_register.csv'
    asyncio.run(save_csv_to_redis(file_path))
    # asyncio.run(delete_all_transactions())
    

