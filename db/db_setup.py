import logging
from pymongo import DESCENDING
# from db.db_connection import get_client
from db.db_connection  import mongo_db

# Constants for index fields
ORDERS_INDEX_FIELD = "orderid"
PAYMENTS_INDEX_FIELD = "paymentid"
TERMINALS_INDEX_FIELD = "tid"
EPOS_INDEX_FIELD = "serialid"

# Setup logging
logging.basicConfig(level=logging.INFO)

# Establishing MongoDB connection
client = mongo_db.connect()

if client is None:
    logging.error("Failed to establish a MongoDB client connection.")
    exit(1)  # Exiting the application if connection is not established

# Database and collections setup
pylon = client['pylon']  
Transactions = pylon['transactions']
Payments = pylon['payments']
Terminals = pylon['terminals']
Epos = pylon['epos']
Control = pylon['app_control']

async def is_initialization_done():
    """Checks if the database initialization process has been completed."""
    control_doc = await Control.find_one({"type": "initialization"})
    print(f"control_doc, {control_doc}")
    return control_doc is not None and control_doc.get("completed")


async def mark_initialization_done():
    """Marks the database initialization process as completed."""
    await Control.update_one({"type": "initialization"}, {"$set": {"completed": True}}, upsert=True)

async def create_indices():
    """Creates necessary indices for the collections."""
    collections_and_fields = {
        Transactions: ORDERS_INDEX_FIELD,
        Payments: PAYMENTS_INDEX_FIELD,
        Terminals: TERMINALS_INDEX_FIELD,
        Epos: EPOS_INDEX_FIELD
    }
    for collection, index_field in collections_and_fields.items():
        try:
            existing_indices = await collection.list_indexes().to_list(length=None)
            if not any(idx['key'] == {index_field: DESCENDING} for idx in existing_indices):
                await collection.create_index([(index_field, DESCENDING)], unique=True)
                logging.info(f"Index created for {collection.name} on field {index_field}")
        except Exception as e:
            logging.error(f"Error creating index on {collection.name}: {e}")





