import motor.motor_asyncio
from dotenv import load_dotenv
import logging
import os
from pymongo.errors import PyMongoError


class MongoDB:

    def __init__(self):
        load_dotenv()
        self.client = None 
        self.url = os.getenv("DATABASE_URL")
        
    def connect(self):
        try:
            self.client = motor.motor_asyncio.AsyncIOMotorClient(self.url, tz_aware=True)
            logging.info("Connected to MongoDB")
            return self.client
        except Exception as e:
            logging.error(f"MongoDB Connection Error: {e}")
            return None
        
    def close(self):
        if self.client:
            self.client.close() 
            logging.info("MongoDB connection closed") 
            self.client = None


mongo_db = MongoDB()