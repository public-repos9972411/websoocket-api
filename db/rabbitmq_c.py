import json, os
import sys
import aio_pika
from dotenv import load_dotenv
sys.path.append('/Users/lwanga/Documents/HARK/weza/')
from common.utils_client import JSONEncoder

# Load environment variables
load_dotenv()

FULL_ACCESS_PERMISSION = os.getenv('FULL_ACCESS_PERMISSION')
RABBITMQ_API = os.getenv('RABBITMQ_API')
RABBITMQ_USERNAME = os.getenv('RABBITMQ_USERNAME')
RABBITMQ_PASSWORD = os.getenv('RABBITMQ_PASSWORD')
GATEWAY_URL = os.getenv('GATEWAY_URL')


class RabbitMQConnectionManager:
    def __init__(self):
        self.rabbitmq_connection = None
        self.rabbitmq_channel = None
        self.terminal_id = None

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def connect(self, vhost_name: str):
        try:
            self.rabbitmq_connection = await aio_pika.connect_robust(
                    f"amqp://{RABBITMQ_USERNAME}:{RABBITMQ_PASSWORD}@{RABBITMQ_API}/{vhost_name}")
            self.rabbitmq_channel = await self.rabbitmq_connection.channel()
            print("Connected to RabbitMQ")
            return self.rabbitmq_connection, self.rabbitmq_channel 
        except Exception as e:
            print(f"Failed to connect to RabbitMQ: {e}")
            return None, None # must returb a tuple

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def declare_queue(self, queue_name, durable=True, exclusive=False, auto_delete=False, arguments=None):
        if not self.rabbitmq_channel or self.rabbitmq_channel.is_closed:
            await self.connect()

        # Declare the queue with the given properties
        queue = await self.rabbitmq_channel.declare_queue(
            queue_name,
            durable=durable,
            exclusive=exclusive,
            auto_delete=auto_delete,
            arguments=arguments
        )
        return queue
    
    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def close(self):
        if self.rabbitmq_channel:
            await self.rabbitmq_channel.close()
        if self.rabbitmq_connection:
            await self.rabbitmq_connection.close()
        print("Disconnected from RabbitMQ")
    
    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def publish(self, queue_name, message, **kwargs):
        # Ensure the channel is open
        if not self.rabbitmq_channel or self.rabbitmq_channel.is_closed:
            await self.connect()

        # Declare the queue
        await self.declare_queue(queue_name, durable=False)

        # Convert message to bytes if it's not already
        if isinstance(message, dict):
            # message = json.dumps(message).encode()
            message = json.dumps(message, cls=JSONEncoder)
        
        # Convert message to bytes, specifying the encoding
        message_bytes = message.encode('utf-8')
    
        # Publishing to the default exchange with the queue name as routing key
        await self.rabbitmq_channel.default_exchange.publish(
            aio_pika.Message(
                body=message_bytes, 
                delivery_mode=aio_pika.DeliveryMode.PERSISTENT,
                expiration=int(60000)  # Message expiration time in milliseconds
            ),
            routing_key=queue_name
        )

    # # +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    async def consume(self, queue_name, callback):
       # Ensure the channel is open
        if not self.rabbitmq_channel or self.rabbitmq_channel.is_closed:
            await self.connect()

        queue = await self.rabbitmq_channel.declare_queue(queue_name)
        await queue.consume(callback)


