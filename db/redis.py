import asyncio
import sys, os
from dotenv import load_dotenv

from redis.asyncio import Redis
# from core.setting import settings


# Load environment variables
load_dotenv()

REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD')

# Constants
MAX_RETRIES = 5  # Maximum number of retries
RETRY_INTERVAL = 5  # Seconds to wait between retries

class RedisConnection:
    """
    Manages a Redis connection pool using redis.asyncio.
    """
    def __init__(self):
        self._pool = None

    async def setup(self):
        for attempt in range(MAX_RETRIES):
            try:
                self._pool = Redis(
                    host=REDIS_HOST,
                    port=REDIS_PORT,
                    # username=REDIS_USERNAME,  # Omit if no username is set
                    password=REDIS_PASSWORD,
                    decode_responses=True
                )
                await self._pool.ping()  # Attempt to open the connection and ping
                break  # Break the loop if connection is successful
            except Exception as e:
                print(f"Attempt {attempt + 1} failed: {e}")
                if attempt < MAX_RETRIES - 1:
                    await asyncio.sleep(RETRY_INTERVAL)  # Wait before retrying
                else:
                    print(f"Maximum number {MAX_RETRIES} of connection retries to redis exceeded.")
                    raise e
                    

    async def get_connection(self):
        if self._pool is None:
            await self.setup()
        return self._pool

    async def close(self):
        if self._pool is not None:
            await self._pool.close()

redis_connection = RedisConnection()




